
// para conectar con mlab
const requestJson = require('request-json');

function getProvincias(req, res) {
  console.log("GET /apitechu/provincias");

  var provincias = require('../provincias.json');
  var resultado = {
    "provincias" : provincias
  }
  res.send(resultado);
}

function getDivisas(req, res) {
  console.log("GET /apitechu/divisas");

  var divisas = require('../divisas.json');
  var resultado = {
    "divisas" : divisas
  }
  res.send(resultado);
}

module.exports.getProvincias = getProvincias;
module.exports.getDivisas = getDivisas;
