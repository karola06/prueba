// const io = require ('../io');
const crypt = require("../crypt");
const validador = require("../validador");

// para conectar con mlab
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechucab12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginUser(req, res) {
  var mensaje='';
  console.log("POST /apitechu/login");
//  console.log(req.body.email);
  mensaje=validador.validaemail(req.body.email);
//  console.log(mensaje.msg);
//  console.log(mensaje.status);
  if (mensaje.status=="NOK"){
    res.send(mensaje);
  }
  if (mensaje.status=="OK"){
    mensaje=validador.validapassword(req.body.password);
    console.log("Password" + req.body.password);
    console.log(validador.validapassword(req.body.password));
    if (mensaje.status=="NOK"){
      res.send(mensaje);
    }
    if (mensaje.status=="OK"){

    var httpClient = requestJson.createClient(baseMLABUrl);
//    console.log("Client created");

  // var query ='q={"email":{"$eq":"' + req.body.email +'"}}';
    var query = "q=" + JSON.stringify({"email": req.body.email});
    var response = {};
    console.log("query: " + query);

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMlab1, body){
        console.log(body);
  //      console.log(body.length);
        if (body.length > 0 ){
          if (crypt.checkPassword(req.body.password,body[0].password)){
            var putBody = '{"$set":{"logged":true}}';
            query ='q={"id":' + Number.parseInt(body[0].id) + '}';  // Se convierte a entero es conveniente
            console.log("query: " + query);
            httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
              function(err, resMlab2, body2){
                response = {
                  "status": "OK",
                  "msg":"Usuario logado",
                  "idUsuario" : body[0].id
                }
                res.send(response);
              }
            )
          }
          else {
            response = {
              "status": "NOK",
              "msg":"Login incorrecto"
            }
            res.send(response);
          }
        } else{
            response = {
              "status": "NOK",
              "msg":"Login incorrecto"
            }
            res.send(response);
        }
      }
  );
  }
  }
  console.log("FIN----POST /apitechu/login");
}

function logoutUser(req, res) {
  console.log("POST /apitechu/logout");
  var resultado = {};
  var httpClient = requestJson.createClient(baseMLABUrl);
//  var query ='q={"$and":[{"id":' + req.params.id + '} , {"logged":true}]}';
  var query ='q={"$and":[{"id":' + Number.parseInt(req.params.id) + '} , {"logged":true}]}';
  var response = {};
  console.log("Client created");
  console.log("query: " + query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab1, body){
        console.log("longitud " + body.length);
        if (body.length == 0 ){
          response = {"msg":"Logout incorrecto"}
          res.send(response);
        } else {
            query ='q={"id":' + req.params.id +'}}';
            var putBody = '{"$unset":{"logged":""}}'

            console.log("query: " + query);

            httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
              function(err, resMlab2, body){
                response = {"msg":"Usuario logout"};  //body[0]
                res.send(response);
              }
            )
        }
    }
  )
}

module.exports.loginUser = loginUser;
module.exports.logoutUser = logoutUser;
