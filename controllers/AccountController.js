const crypt = require("../crypt");

// para conectar con mlab
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechucab12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getCuentasById (req, res){
  console.log("GET /apitechu/cuentas/:id");

  var id = req.params.id;
  console.log("La id del usuario a obtener es: " + id);
  var query ='q={"idUsuario":' + Number.parseInt(id) +'}';

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      if (err) {
        var response = {
            "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      } else{
        if (body.length > 0){
          console.log(body);
          var response = body;
        } else{
          var response = {
                "msg" : "Usuario no encontrado"
              }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function crearCuenta(req, res){
  console.log("POST /apitechu/cuenta/:id");
  console.log(req.params.id);
// todas las cuentas son del banco 3333 y la sucursal 6666 los digitos de control son siempre 99
//   ES99 3333 6666 99
  var query = "q= {'IBAN' : {$regex : 'ES99 3333 6666 99'}}&s={IBAN:-1}&f={_id:0,IBAN:1}&l=1";
  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      // body devuelve un array */
      if (err) {
        var response = {
          "msg" : "Error obteniendo máxima cuenta"
            }
        res.status(500);
        res.send(response);
      } else{
          console.log(body[0]);
          var ibanmax = body[0].IBAN;
          console.log(ibanmax.replace(/ /g, "").substring(14));
          console.log(Number.parseInt(ibanmax.replace(/ /g, "").substring(14)));
          var siguiente= Number.parseInt((ibanmax.replace(/ /g, "").substring(14)))+1;
//          console.log(siguiente);
          var zero = "0";
          var ctaaux=siguiente.toString();
//          console.log(ctaaux);
          ctaaux = zero.repeat(10 - ctaaux.length) + ctaaux;
//          console.log(ctaaux);
          ctaaux =ctaaux.substring(0,2) + ' ' + ctaaux.substring(2,6) + ' ' + ctaaux.substring(6,10);
//          console.log(ctaaux);
//          cuentasig = body[0].IBAN.substring(0,17) + siguiente.zfill(10);
          ctasig = ibanmax.substring(0,17) + ctaaux;
/*          var siguiente ='ES99 3333 6666 9900 0000 0001';*/
          console.log("siguiente cuenta " + ctasig);
          var newCuenta = {
            "IBAN": ctasig,
            "idUsuario": Number.parseInt(req.params.id),
            "balance": "0.00"
          }
//        Se da de alta la cuenta
          httpClient.post("cuentas?" + mLabAPIKey, newCuenta,
            function(err, resMlab, body2){
              console.log("Cuenta creada");
              response = body2[0];
              res.status(201);
              res.send({"msg":"cuenta creada"});
            }
          )
        }
      }
  );
}

function actSaldoCuenta(req, res) {
  console.log("POST /apitechu/actsaldo");
  console.log("Actualizar Saldo");
  console.log(req.body.iban);
  console.log(req.body.importe);

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  // var query ='q={"email":{"$eq":"' + req.body.email +'"}}';
  var query = "q=" + JSON.stringify({"IBAN": req.body.iban});
  var response = {};
  var importenuevo="";
  console.log("query: " + query);

  httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
    function(err, resMlab1, body){
        console.log(body);
        console.log("********************");
        if (body.length > 0 ){
          console.log(Number.parseInt(body[0].balance));
          console.log(Number.parseInt(req.body.importe));
          if (req.body.importe==!"+" && req.body.importe==!"-"){
            response = {"msg":"Tipo de movimiento incorrecto"};
            res.status(401);
            res.send(response);
          } else {
            console.log("Tipo de movimiento " + req.body.tipomov);
/*            if (req.body.tipomov=="+"){
              var importenuevo= Number.parseInt(body[0].balance) + Number.parseInt(req.body.importe);
            } else {
              var importenuevo= Number.parseInt(body[0].balance) - Number.parseInt(req.body.importe);
            }  */
            let saldo = Number.parseFloat(body[0].balance,10).toFixed(2);
            console.log("saldo " + saldo);
            importe = Number.parseFloat(req.body.importe,10).toFixed(2);
            console.log("importe " + importe);
            if (req.body.tipomov=="+"){
              importenuevo= Number.parseFloat(saldo) + Number.parseFloat(importe);
            } else {
              importenuevo= Number.parseFloat(saldo) - Number.parseFloat(importe);
            }
            importenuevo=Number.parseFloat(importenuevo,10).toFixed(2);

          }
            console.log("importe nuevo " + importenuevo);
            var putBody = '{"$set": ' + JSON.stringify({"balance": importenuevo}) + '}';
            console.log("putBody " + putBody);
//            query ='q={"id":' + body[0].id + '}';
            query ='q={"IBAN": "' + req.body.iban + '"}';  // Se convierte a entero es conveniente
            console.log("query: " + query);
            httpClient.put("cuentas?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
              function(err, resMlab2, body2){
                response = {"msg":"Saldo actualizado"
                          }
                res.send(response);
              }
            )
          }
          else{
            response = {"msg":"Cuenta incorrecto"}
            res.status(401);
            res.send(response);
          }
      }
  );
}

module.exports.getCuentasById = getCuentasById;
module.exports.crearCuenta = crearCuenta;
module.exports.actSaldoCuenta = actSaldoCuenta;
