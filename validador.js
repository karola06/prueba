
function validaemail(email){
  console.log("Validando email");
  var response = {};

  if (email != null && email !=''){
    if (email.indexOf('@') == -1) {
      response = {
        "status": "NOK",
        "msg": "El email debe de contener una @"
      };
      return(response);
    }
    else{
      if (email.indexOf('.') == -1){
        response = {
          "status": "NOK",
          "msg": "El email debe de contener un ."
        };
        return(response);
      }
      else{
        response = {"status": "OK"};
        return(response);
      }
    }
  }
  else {
    response = {
      "status": "NOK",
      "msg": "El email es obligatorio"
    };
    return(response);
  }
}

function validapassword(password){
  console.log("Validando password");
  var response = {};

  if (password != null && password !=''){
//    console.log("paso1 " + password.length);
    if (password.length < 6) {
      response = {
        "status": "NOK",
        "msg": "El password debe de tener una longitud de al menos 6 carácteres"
      };
      return(response);
    } else {
      response = {
        "status": "OK",
        "msg": "Datos correctos"
      };
      return(response);
    }
  } else {
    response = {
      "status": "NOK",
      "msg": "El password es obligatorio"
    };
    return(response);
  }
}

function validacodigopostal(codpostal){
  if (isNaN(codpostal) && (codpostal%1 == 0) && ((codpostal/10000)%1 == 0)) {
    response = {
      "status": "OK",
      "msg": "Datos correctos"
    };
    return(response);
  } else {
  response = {
    "status": "NOK",
    "msg": "El código postal debe de ser un numérico de cinco"
  };
  return(response);
  }
}

function validafecha(texto) {
  console.log("fecha " + texto);
//   let partes = /^(\d{1,2})[/](\d{1,2})[/](\d{3,4})$/.exec(texto);
   let partes = /^(\d{3,4})[-](\d{1,2})[-](\d{1,2})$/.exec(texto);

    if (!partes) {
      response = {
        "status": "NOK",
        "msg": "La fecha es inválida"
      };
      return(response);
    }

/*    let d = parseInt(partes[1], 10),
        m = parseInt(partes[2], 10),
        a = parseInt(partes[3], 10);  */

        let a = parseInt(partes[1], 10),
            m = parseInt(partes[2], 10),
            d = parseInt(partes[3], 10);


    //Validar mes
    if (!a || !m || m > 12 || !d){
      response = {
        "status": "NOK",
        "msg": "La fecha es inválida"
      };
      return(response);
    }

    let diasPorMes = [31,28,31,30,31,30,31,31,30,31,30,31 ];

    //Si es bisiesto, febrero tiene 29
    if (m == 2 && (a % 4 == 0 && a % 100 != 0) || a % 400 == 0){
      diasPorMes[1] = 29;
    }

    //Que no tenga más días de los permitidos en el mes
    if (d > diasPorMes[m - 1]) {
      response = {
        "status": "NOK",
        "msg": "La fecha es inválida"
      };
      return(response);
    } else {
      response = {
        "status": "OK",
        "msg": "Datos correctos"
      };
      return(response);
    }
}

function validatipomov(tipomov) {
  console.log("tipomov " + tipomov);
  if (tipomov != '+' && tipomov !='-') {
    response = {
      "status": "NOK",
      "msg": "El tipo de movimiento es inválido"
    };
    return(response);
  } else {
    response = {
      "status": "OK",
      "msg": "Datos correctos"
    };
    return(response);
  }
}

function validaimporte(texto) {
  console.log("importe " + texto);
  let partes = /^(\d{1,6})[.](\d{2})$/.exec(texto);
  if (!partes) {
    response = {
    "status": "NOK",
    "msg": "El importe es inválido. Debe tener dos decimales"
    };
    return(response);
  } else {
//    int cadenaResultadoInt = Integer.parseInt(texto);
    console.log(parseFloat(texto,10).toFixed(2));
    response = {
      "status": "OK",
      "msg": "Datos correctos"
    };
    return(response);
  }
}

function validadatosusuario(datosusuario){
  console.log("Validando datos de usuario");
  var response={};
  console.log(datosusuario.nombre);
  console.log(datosusuario.apellidos);
  console.log(datosusuario.email);
  console.log(datosusuario.direccion);
  console.log(datosusuario.provincia);
  console.log(datosusuario.codpostal);
  console.log(datosusuario.password);
  if (datosusuario.nombre == null || datosusuario.nombre ==''){
    response = {
      "status": "NOK",
      "msg": "El nombre es obligatorio"
    };
    return(response);
  }
  if (datosusuario.apellidos == null || datosusuario.apellidos ==''){
    response = {
      "status": "NOK",
      "msg": "Los apellidos son obligatorios"
    };
    return(response);
  }
  if (datosusuario.email == null || datosusuario.email ==''){
    response = {
      "status": "NOK",
      "msg": "El email es obligatorio"
    };
    return(response);
  }
  if (datosusuario.direccion == null || datosusuario.direccion ==''){
    response = {
      "status": "NOK",
      "msg": "La direccion es obligatoria"
    };
    return(response);
  }
  if (datosusuario.provincia == null || datosusuario.provincia ==''){
    response = {
      "status": "NOK",
      "msg": "La provincia es obligatoria"
    };
    return(response);
  }
  if (datosusuario.codpostal == null || datosusuario.codpostal ==''){
    response = {
      "status": "NOK",
      "msg": "El código postal es obligatorio"
    };
    return(response);
  }
  if (datosusuario.password == null || datosusuario.password ==''){
    response = {
      "status": "NOK",
      "msg": "El password es obligatorio"
    };
    return(response);
  } else {
    response = {
      "status": "OK",
      "msg": "Datos correctos"
    };
    return(response);
  }
}

function validadatosmovimiento(datosmovimiento){
  console.log("Validando datos de movimiento");
  console.log(datosmovimiento.idcuenta);
  console.log(datosmovimiento.fechamov);
  console.log(datosmovimiento.concepto);
  console.log(datosmovimiento.tipomov);
  console.log(datosmovimiento.importe);
  if (datosmovimiento.idcuenta == null || datosmovimiento.idcuenta ==''){
    response = {
      "status": "NOK",
      "msg": "La cuenta es obligatoria"
    };
    return(response);
  }
  if (datosmovimiento.fechamov == null || datosmovimiento.fechamov ==''){
    response = {
      "status": "NOK",
      "msg": "La fecha del movimiento es obligatoria"
    };
    return(response);
  }
  if (datosmovimiento.concepto == null || datosmovimiento.concepto ==''){
    response = {
      "status": "NOK",
      "msg": "El concepto es obligatorio"
    };
    return(response);
  }
  if (datosmovimiento.tipomov == null || datosmovimiento.tipomov ==''){
    response = {
      "status": "NOK",
      "msg": "El tipo de movimiento es obligatorio"
    };
    return(response);
  }
  if (datosmovimiento.importe == null || datosmovimiento.importe ==''){
    response = {
      "status": "NOK",
      "msg": "El importe del movimiento es obligatorio"
    };
    return(response);
  } else {
    response = {
      "status": "OK",
      "msg": "Datos correctos"
    };
    return(response);
  }
}

module.exports.validaemail = validaemail;
module.exports.validapassword = validapassword;
module.exports.validacodigopostal = validacodigopostal;
module.exports.validafecha = validafecha;
module.exports.validatipomov = validatipomov;
module.exports.validaimporte = validaimporte;
module.exports.validadatosusuario = validadatosusuario;
module.exports.validadatosmovimiento = validadatosmovimiento;
